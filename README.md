# Denver Checkbook Visual

All [data](http://data.denvergov.org/dataset/city-and-county-of-denver-checkbook) is from [Denver Open Data Catalog](http://data.denvergov.org/)
licensed under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/legalcode).

## Prerequisites

### CouchDB

Either install [locally](http://couchdb.apache.org/)
or use a [service](https://cloudant.com/).

### Nodejs

[Install](https://nodejs.org/)

### npm libraries

```shell
$ npm install
```

## Usage

### Configure

Modify the values in `config.json` to point to your CouchDB and Checkbook.csv.

### Populate Database

```bash
$ npm run populate
```
