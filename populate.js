var http = require('http');
var stream = require('stream');
var Writable = stream.Writable;
var csv = require('csv');
var Promise = require('es6-promise').Promise;
var config = require('./config.json');
var db = require('./db.js');

var total = 0;

function writeStdoutLine (message) {
	process.stdout.clearLine();
	process.stdout.cursorTo(0);
	process.stdout.write(message);
}

var saveToDb = new Writable({ objectMode: true });
saveToDb._write = function (data, enc, next) {
	db.insert(data, next)
};

db.open(config.couchdbUrl, config.couchdbName)
	.then(function () {
		return new Promise(function (resolve, reject) {
			http.get(config.dataUrl, function (res) {
				res
					.pipe(csv.parse({
						relax: true,
						columns: true
					}))
					.on('data', function () {
						total = total + 1;
						if (total % 1000 === 0) {
							writeStdoutLine('Rows read: ' + total);
						}
					})
					.pipe(saveToDb)
					.on('finish', db.flush)
					.on('finish', resolve)
					.on('error', reject);
			})
			.on('error', reject);
		});
	})
	.then(function () {
		process.stdout.write('\n');
		console.info('Inserted ' + total + ' documents.');
		console.log('Done populating the database. :)');
	})
	.catch(function (error) {
		console.error(error);
		console.error(error.stack);
	});
