var nano = require('nano');
var _ = require('lodash');
var Promise = require('es6-promise').Promise;
var couch = null;
var db = null;

function open (couchdbUrl, couchdbName) {
	return new Promise(function (resolve, reject) {
			resolve(openDatabase(couchdbUrl));
		})
		.then(function () {
			return listDatabases();
		})
		.then(function (dbs) {
			return createDatabase(dbs, couchdbName);
		})
		.then(function () {
			return insertDesignDocuments(couchdbName);
		});
}

function openDatabase (couchdbUrl) {
	return new Promise(function (resolve, reject) {
			console.log('Connecting to "' + couchdbUrl + '"...');
			nano = nano(couchdbUrl);
			resolve();
		});
}

function listDatabases () {
	return new Promise(function (resolve, reject) {
			console.log('Getting list of databases...');
			nano.db.list(function (err, body) {
				if (err) {
					reject(err);
					return;
				}
				resolve(body);
			});
		});
}

function createDatabase (dbs, couchdbName) {
	return new Promise(function (resolve, reject) {
		if (!_.contains(dbs, couchdbName)) {
			console.log('Creating database named "' + couchdbName + '"...');
			nano.db.create(couchdbName, function (err, body) {
				if (err) {
					reject(err);
					return;
				}
				console.log('Database named "' + couchdbName + '" created.');
				db = nano.use(couchdbName);
				resolve();
			});
			return;
		}
		console.log('Database named "' + couchdbName + '" already exists.');
		db = nano.use(couchdbName);
		resolve();
	})
}

function insertDesignDocuments (couchdbName) {
	return new Promise(function (resolve, reject) {
		var builder = require("view-builder");
		console.log('Inserting Design Documents...');
		var designDocs = require('./views.js');
		builder(nano.config.url + '/' + couchdbName, designDocs);
		resolve();
	});
}

var schema = {
	RowId: 'id',
	Payee: 'string',
	City: 'string',
	State: 'string',
	ProgramArea: 'string',
	PaymentId: 'number',
	Year: 'number',
	Amount: 'number',
	PurchaseOrder: 'string',
	FundingSourceDescription: 'string',
	Project: 'string',
	ProjectDescription: 'string',
	ExpenseCategory: 'string',
	ExpenseSubCategory: 'string'
};

function convertToDoc (row) {
	return _.transform(schema, function (doc, type, key) {
		if (type === 'id') {
			doc['_id'] = row[key];
			doc[key] = row[key];
		}
		if (type === 'string') {
			doc[key] = row[key];
		}
		if (type === 'number') {
			doc[key] = parseFloat(row[key]);
		}
		if (type === 'date') {
			throw new Error('Date type not implemented!');
		}
	});
}

var queue = [];
var batchSize = 600;

function insert (row, callback) {
	var doc = convertToDoc(row);
	queue.push(doc);
	if (queue.length >= batchSize) {
		var batch = _.take(queue, batchSize);
		queue = _.drop(queue, batchSize);
		db.bulk({docs: batch}, function (err, body) {
			if (err) {
				console.error(err);
				throw err;
			}
			callback();
		});
		return;
	}
	callback();
}

function flush () {
	var batch = queue;
	queue = [];
	db.bulk({docs: batch}, function (err, body) {
		if (err) {
			console.error(err);
			throw err;
		}
	});
}

module.exports = {
	open: open,
	insert: insert,
	flush: flush
}
