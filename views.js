module.exports = {
	'totals': {
		'views': {
			'amountByState': {
				'map': function (doc) {
					if (doc.State) {
						emit(doc.State, doc.Amount);
					}
				},
				'reduce': '_sum'
			}
		}
	}
};
